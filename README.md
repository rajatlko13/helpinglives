# HelpingLives
Blockchain based web application for __CHAINLINK HACKATHON__.

## Contents
- __[Description](#description)__
- __[Tech Stack](#tech-stack)__
- __[Use of Blockchain](#use-of-blockchain)__

### Description

I have built a blockchain based web application that provides user real cryptocurrency whenever they donate blood or organs. This will facilitate the people to donate blood more frequently as they will get rewarded with real ethers. I have also included the feature of reporting the doctors where a user/patient can report a hospital/doctor if they think that the doctor is doing unregistered medical practice (like happens in most of the villages), or if the hospital is charging unnecessary fees. If we get some minimum number of reports against the doctor then we will forward this case to the higher concerned authorities so that they can take strict action against such doctors.

### Tech Stack

- MERN stack
- Bootstrap
- Semantic UI React
- Ethereum Blockchain
- Solidity
- Infura  
- Metamask

### Use of Blockchain

I created __[ERC20](https://ethereum.org/en/developers/docs/standards/tokens/erc-20/)__ standard tokens for our application and named them __HealthCoin(HC)__. These tokens will be used to reward the users whenever they will donate blood or organ. The user can then convert these HealthCoins to real cryptocurrency ether, which are used for transactions on the ethereum blockchain. I have created a Healthcare __[smart contract](https://ethereum.org/en/developers/docs/smart-contracts/)__ which will govern the distribution of HealthCoins to the user whenever the blood bank confirms that the person has successfully donated the blood, and same goes for the organ donation. 

